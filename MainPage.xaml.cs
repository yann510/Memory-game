﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AppMajorChallenge4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private List<Button> buttons = new List<Button>();
        private List<Button> playButton;
        private int score = 0;
        private int maximumScore = 0;

        public MainPage()
        {
            this.InitializeComponent();
            for (int i = 0; i < GameGrid.Children.Count; i++)
            {
                var child = GameGrid.Children[i];
                var b = (Button) child;
                b.Name = $"button{i}";
                buttons.Add(b);
            }
            
            PreStart();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button) sender;
            if (b.Content != "START")
            {
                if (playButton.Contains(b))
                {
                    playButton.Remove(b);
                    b.Background = new SolidColorBrush(Colors.Red);
                    score++;
                    if (score == maximumScore)
                    {
                        EndGame();
                    }
                }
                else
                {
                    EndGame();
                }
            }
        }

        private void PreStart()
        {
            score = 0;
            maximumScore = 0;
            playButton = new List<Button>();
            var startButton = buttons.Single(x => x.Name == "button24");
            startButton.Content = "START";
            startButton.Foreground = new SolidColorBrush(Colors.Black);
            startButton.Click += StartButton_Click;
            foreach (var button in buttons)
            {
                button.Background = new SolidColorBrush(Colors.Aqua);
            }
        }

        private async void StartButton_Click(object sender, RoutedEventArgs e)
        {
            await StartGame();
        }

        private async Task StartGame()
        {
            var scoreButton = buttons.Single(x => x.Name == "button0");
            var scoreResultButton = buttons.Single(x => x.Name == "button1");
            var winButton = buttons.Single(x => x.Name == "button7");

            scoreButton.Content = "";
            scoreResultButton.Content = "";
            winButton.Content = "";

            var startButton = buttons.Single(x => x.Name == "button24");
            startButton.Click -= StartButton_Click;
            startButton.Content = "";
            startButton.Background = new SolidColorBrush(Colors.Aqua);
            var random = new Random();
            int numberOfButtons = random.Next(5,buttons.Count);
            for (int i = 0; i < numberOfButtons; i++)
            {
                int randomButton = random.Next(buttons.Count);
                var button = buttons.Single(x => x.Name == $"button{randomButton}");
                if (!playButton.Contains(button))
                {
                    playButton.Add(button);
                }
            }
            maximumScore = playButton.Count;
            foreach (var button in playButton)
            {
                button.Background = new SolidColorBrush(Colors.Red);
                button.Click -= Button_Click;
            }

            await Task.Delay(3000);

            foreach (var button in playButton)
            {
                button.Background = new SolidColorBrush(Colors.Aqua);
                button.Click += Button_Click;
            }
        }

        private void EndGame()
        {
            if (maximumScore > 0)
            {
                var scoreButton = buttons.Single(x => x.Name == "button0");
                var scoreResultButton = buttons.Single(x => x.Name == "button1");
                var winButton = buttons.Single(x => x.Name == "button7");

                scoreButton.Foreground = new SolidColorBrush(Colors.Black);
                scoreButton.Content = "Score:";

                scoreResultButton.Foreground = new SolidColorBrush(Colors.Black);
                scoreResultButton.Content = $"{score} / {maximumScore}";
                if (score == maximumScore)
                {
                    winButton.Foreground = new SolidColorBrush(Colors.Black);
                    winButton.Content = "You Won!!";
                }
                PreStart();
            }
        }
    }
}
